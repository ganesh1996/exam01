package com.example.test.interactor;

import com.example.test.listners.onResponseListner;

public interface MainActivityInteractor {

    void getImageData(String api,onResponseListner responseListner);
}
