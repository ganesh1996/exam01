package com.example.test.ViewHolders;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.test.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class AuthorViewHolder extends RecyclerView.ViewHolder {

    ImageView ivAuthor;
    TextView tvAuthor;

    public AuthorViewHolder(@NonNull View itemView) {
        super(itemView);
        ivAuthor = itemView.findViewById(R.id.img_author_image);
        tvAuthor = itemView.findViewById(R.id.txt_author_name);
    }

    public void bindData(String name, String image) {
        tvAuthor.setText(name);
        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                ivAuthor.setImageBitmap((Bitmap) msg.obj);
            }
        };


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(image).getContent());
                    Message message = new Message();
                    message.obj = bitmap;
                    handler.sendMessage(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
