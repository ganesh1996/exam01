package com.example.test.interactorImpl;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.example.test.DataClass.Author;
import com.example.test.R;
import com.example.test.interactor.MainActivityInteractor;
import com.example.test.listners.onResponseListner;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivityInteractorImpl implements MainActivityInteractor {
    @Override
    public void getImageData(String api, onResponseListner responseListner) {
        new AsyncTask<String, Void, List<Author>>() {

            @Override
            protected List<Author> doInBackground(String... strings) {
                try {
                    URL url = new URL(strings[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    InputStream stream = new BufferedInputStream(urlConnection.getInputStream());
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
                    StringBuilder builder = new StringBuilder();
                    String inputString;
                    while ((inputString = bufferedReader.readLine()) != null) {
                        builder.append(inputString);
                    }
                    JSONArray jsonArray = new JSONArray(builder.toString());
                    Log.d("test", "" + jsonArray.length());
                    List<Author> authors = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        //Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL("https://picsum.photos/300/300?image=" + object.optInt("id")).getContent());
                        /*HashMap<String, Bitmap> innerHashMap = new HashMap<>();
                        innerHashMap.put(object.optString("author"), null);
                        data.put(object.getInt("id"), innerHashMap);*/

                        Author author = new Author();
                        author.setAuthor_name(object.optString("author"));
                        author.setId(object.getInt("id"));
                        author.setImageUrl("https://picsum.photos/300/300?image=" + object.optInt("id"));
                        authors.add(author);
                    }
                    return authors;
                } catch (Exception e) {
                    Log.d("error", e.toString());
                    responseListner.onFailure(e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(List<Author> authorList) {
                super.onPostExecute(authorList);
                responseListner.onSuccess(authorList);
            }
        }.execute(api);
    }
}
