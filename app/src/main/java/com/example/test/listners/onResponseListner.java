package com.example.test.listners;

public interface onResponseListner {

    void onSuccess(Object data);

    void onFailure(Exception e);
}
