package com.example.test.listners;

public interface onDataChangeListner {

    void onDataReceived(Object o);

    void onDataNotReceived(Exception e);
}
