package com.example.test.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.test.Adapaters.AuthorAdapater;
import com.example.test.DataClass.Author;
import com.example.test.R;
import com.example.test.listners.onDataChangeListner;
import com.example.test.presenter.MainActivityPresenter;
import com.example.test.presenterImpl.MainActivityPresentorImpl;

import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity implements onDataChangeListner {
    MainActivityPresenter mainActivityPresenter;
    List<Author> data;
    ProgressBar progressBar;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivityPresenter = new MainActivityPresentorImpl(this);
        mainActivityPresenter.getData(getResources().getString(R.string.api));
    }

    @Override
    public void onDataReceived(Object o) {
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                data = (List<Author>) o;
                //Toast.makeText(MainActivity.this, ""+data.size(), Toast.LENGTH_LONG).show();
                Log.d("size", "" + data.size());
                init();
            }
        });

    }

    @Override
    public void onDataNotReceived(Exception e) {
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void init() {
        progressBar = findViewById(R.id.progress_circular);
        recyclerView = findViewById(R.id.recycler_view);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(MainActivity.this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        AuthorAdapater authorAdapater = new AuthorAdapater(MainActivity.this, data);
        recyclerView.setAdapter(authorAdapater);
        progressBar.setVisibility(View.GONE);
    }
}
