package com.example.test.presenterImpl;

import com.example.test.interactor.MainActivityInteractor;
import com.example.test.interactorImpl.MainActivityInteractorImpl;
import com.example.test.listners.onDataChangeListner;
import com.example.test.listners.onResponseListner;
import com.example.test.presenter.MainActivityPresenter;

public class MainActivityPresentorImpl implements MainActivityPresenter, onResponseListner {

    MainActivityInteractor mainActivityInteractor;
    onDataChangeListner onDataChangeListner;

    public MainActivityPresentorImpl(onDataChangeListner onDataChangeListner) {
        mainActivityInteractor = new MainActivityInteractorImpl();
        this.onDataChangeListner = onDataChangeListner;
    }


    @Override
    public void getData(String api) {
        mainActivityInteractor.getImageData(api, this);
    }

    @Override
    public void onSuccess(Object data) {
        onDataChangeListner.onDataReceived(data);
    }

    @Override
    public void onFailure(Exception e) {
        onDataChangeListner.onDataNotReceived(e);
    }
}
