package com.example.test.Adapaters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.test.DataClass.Author;
import com.example.test.R;
import com.example.test.ViewHolders.AuthorViewHolder;

import java.util.HashMap;
import java.util.List;

public class AuthorAdapater extends RecyclerView.Adapter<AuthorViewHolder> {
    Context context;
    List<Author> data;

    public AuthorAdapater(Context context, List<Author> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public AuthorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_image, parent, false);
        return new AuthorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AuthorViewHolder holder, int position) {
        Author author = data.get(position);
        holder.bindData(author.getAuthor_name(), author.getImageUrl());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
