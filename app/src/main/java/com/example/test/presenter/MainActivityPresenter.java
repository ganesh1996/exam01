package com.example.test.presenter;

public interface MainActivityPresenter {

    void getData(String api);
}
